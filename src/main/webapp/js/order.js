$(document).ready(function() {

    window.countBook = 0;

    function init() {
        $("#bookImport .importClass").click(function() {
            var currentRow = $(this).closest("tr");
            var idRow = currentRow.children("td").get(0);
            var nameRow = currentRow.children("td").get(1);
            var genreRow = currentRow.children("td").get(2);
            var authorRow = currentRow.children("td").get(3);
            var publisherRow = currentRow.children("td").get(4);
            var yearRow = currentRow.children("td").get(5);
            var descriptionRow = currentRow.children("td").get(6);
            var translaterRow = currentRow.children("td").get(7);

            var id = $(idRow).html();
            var name = $(nameRow).html();
            var genre = $(genreRow).html();
            var author = $(authorRow).html();
            var publisher = $(publisherRow).html();
            var year = $(yearRow).html();
            var description = $(descriptionRow).html();
            var translater = $(translaterRow).html();
            var orderItems = "";
            if (window.countBook < 5) {
                orderItems += '<tr>';
                orderItems += '<td width="5%"><input type="text" class="form-control bookIdClass" name="books[' + countBook + '].id" placeholder="book.id" required="required" value="' + id + '" size="" readonly /></td>';
                orderItems += '<td width="15%"><input type="text" class="form-control" name="books[' + countBook + '].name" placeholder="book.name" required="required" value="' + name + '" size="" readonly /></td>';
                orderItems += '<td width="10%"><input type="text" class="form-control" name="books[' + countBook + '].genre" placeholder="book.genre" required="required" value="' + genre + '" size="" readonly /></td>';
                orderItems += '<td width="15%"><input type="text" class="form-control" name="books[' + countBook + '].author" placeholder="book.author" required="required" value="' + author + '" size="" readonly /></td>';
                orderItems += '<td width="15%"><input type="text" class="form-control" name="books[' + countBook + '].publisher" placeholder="book.publisher" required="required" value="' + publisher + '" size="" readonly /></td>';
                orderItems += '<td width="10%"><input type="text" class="form-control" name="books[' + countBook + '].year" placeholder="book.year" required="required" value="' + year + '" size="" readonly /></td>';
                orderItems += '<td width="20%"><input type="text" class="form-control" name="books[' + countBook + '].description" placeholder="book.description" required="required" value="' + description + '" size="" readonly /></td>';
                orderItems += '<td width="10%"><input type="text" class="form-control" name="books[' + countBook + '].translater" placeholder="book.translater" required="required" value="' + translater + '" size="" readonly /></td>';
                orderItems += '<td width="5%"><a id="linkOrderItemDeleteId" class="btn btn-danger"><span class="glyphicon glyphicon-minus" aria-hidden="true" ></span></a></td>';
                orderItems += '</tr>';
                window.countBook++;
            }
            $('#items').append(orderItems);
        });
    };

    $("#orderItem").on('click', '#linkOrderItemDeleteId', function() {
        if (confirm("Вы подтверждаете удаление?")) {
            var currentRow = $(this).closest("tr");
            var orderId = document.getElementById('orderform').value;
            var idRow = currentRow.children("td").get(0);
            var bookId = $(idRow).children(".bookIdClass").val();

            if (typeof orderId !== 'undefined') {
	            $.ajax({
	                type: "GET",
	                url: "/order/" + orderId + "/delete/orderItem/" + bookId,
	                cache: false,
	            }).done(function(responce) {
	            }).fail(function(responce) {
	            });
            }
            $(this).closest("tr").remove();
        }
    });

    init();

});
