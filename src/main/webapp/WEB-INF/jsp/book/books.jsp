<%@ page contentType="text/html; charset=UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ui"%>
<ui:html title="Books">
<ui:navbar active="books book.books" />
<ui:panel>
	<ui:panel-header>
		<form class="form-inline">
			<div class="row">
				<div class="form-group col-md-offset-2 col-md-8" align="center">
					<h4>
						<ui:label value="books" />
					</h4>
				</div>
			</div>
		</form>
	</ui:panel-header>
	<div style= "position: fixed;right: 10px;top: 20%;padding: 10px;">
		<ui:abbr title="abbr.up">
			<ui:button href="javascript:void(0)" onClick="window.scrollTo(0,0)" glyph="eject" />
		</ui:abbr>
	</div>
	<div class="row">
		<div class="form-group col-md-10">
	<c:if test="${not empty books}">
		<table class="table table-hover">
			<thead>
				<tr>
					<th scope="col">Id</th>
					<th scope="col">Name</th>
					<th scope="col">Genre</th>
					<th scope="col">Author</th>
					<th scope="col">Publisher</th>
					<th scope="col">Year</th>
					<th scope="col">Description</th>
					<th scope="col">Translator</th>
				</tr>
			</thead>
			<c:forEach var="book" items="${books}">
				<tr>
					<td>${book.id}</td>
					<td>${book.name}</td>
					<td>${book.genre}</td>
					<td>${book.author}</td>
					<td>${book.publisher}</td>
					<td>${book.year}</td>
					<td>${book.description}</td>
					<td>${book.translater}</td>
				<tr>
			</c:forEach>
		</table>
	</c:if>
	</div>
		</div>
	<ui:panel-footer>
	</ui:panel-footer>
</ui:panel>
</ui:html>
