<%@ page contentType="text/html; charset=UTF-8" trimDirectiveWhitespaces="true"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ui"%>

<ui:html title="book">
<body>
	<ui:navbar active="books book.newBook" />
	<div class="row">
		<div class="col-md-offset-2 col-md-8">
			<ui:panel type="info"
				title="${empty book.id ? 'book.add' : 'book.edit'}">
				<c:url var="bookhref" value="/book/add"/>
				<form:form action="${bookhref}" method="POST" id="bookform"
						modelAttribute="book" enctype="application/form-data" >
				<form:input type="hidden" path="id" />
				<table class="table table-condensed none-margin">
					<tr>
						<td width="20%"><ui:label value="book.name" /></td>
						<td width="70%" colspan="3"><ui:input name="name"
							placeholder="book.name.placeholder" cssClass="form-control"
							required="required" value="${book.name}" /></td>
					</tr>
					<tr>
						<td width="20%"><ui:label value="book.description" /></td>
						<td width="70%" colspan="3">
							<textarea class="form-control" rows="3" cols="120" name="description">${book.description}</textarea>
						</td>
					</tr>
					<tr>
						<td width="20%"><ui:label value="book.author" /></td>
						<td width="70%" colspan="3"><ui:input name="author"
							placeholder="book.author.placeholder" cssClass="form-control"
							required="required" value="${book.author}" /></td>
					</tr>
					<tr>
						<td width="20%"><ui:label value="book.genre" /></td>
						<td width="70%" colspan="3"><ui:input name="genre"
							placeholder="book.genre.placeholder" cssClass="form-control"
							required="required" value="${book.genre}" /></td>
					</tr>
					<tr>
						<td width="20%"><ui:label value="book.publisher" /></td>
						<td width="70%" colspan="3"><ui:input name="publisher"
							placeholder="book.publisher.placeholder" cssClass="form-control"
							required="required" value="${book.publisher}" /></td>
					</tr>
					<tr>
						<td width="20%"><ui:label value="book.translater" /></td>
						<td width="70%" colspan="3"><ui:input name="translater"
							placeholder="book.translater.placeholder" cssClass="form-control"
							required="required" value="${book.translater}" /></td>
					</tr>
					<tr>
						<td width="20%"><ui:label value="book.year" /></td>
						<td width="70%">
							<ui:date name="year"
								placeholder="book.year.placeholder"
								cssClass="form-control"
								required="required"
								value="${book.year}" />
						</td>
					</tr>
				</table>
				<ui:panel-footer>
					<div class="btn-group">
						<ui:buttonSubmit name="save Book" type="primary" path="submit" />
					</div>
				</ui:panel-footer>
			</form:form>
			</ui:panel>
		</div>
	</div>
</body>
</ui:html>
