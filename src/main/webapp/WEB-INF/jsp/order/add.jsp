<%@ page contentType="text/html; charset=UTF-8"
	trimDirectiveWhitespaces="true"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ui"%>

<ui:html title="order">
<body>
	<ui:navbar active="orders order.newOrder" />
	<div class="row">
		<div>
			<ui:panel type="info" title="${empty order.id ? 'order.add' : 'order.edit'}">
			<div class="form-group">
					<button title="import.books" type="button" class="btn btn-success" data-backdrop="static" data-toggle="modal" data-target="#myModal"><ui:glyph name="import" /></button>
					<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<form action="" method="get" id="importBook" class="form-inline">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Закрыть">
											<span aria-hidden="true">X</span>
										</button>
										<h4 class="modal-title" id="myModalLabel">order.form.import.books</h4>
									</div>
									<div class="modal-body">
											<div class="row">
												<div class="padding-for-table">
													<table id="bookImport" class="table table-hover">
														<thead>
															<tr>
																<th scope="col">Id</th>
																<th scope="col">Name</th>
																<th scope="col">Genre</th>
																<th scope="col">Author</th>
																<th scope="col">Publisher</th>
																<th scope="col">Year</th>
																<th scope="col">Description</th>
																<th scope="col">Translator</th>
																<th scope="col">Action</th>
															</tr>
														</thead>
														<tbody>
														<c:forEach var="book" items="${books}">
															<tr>
																<td>${book.id}</td>
																<td>${book.name}</td>
																<td>${book.genre}</td>
																<td>${book.author}</td>
																<td>${book.publisher}</td>
																<td>${book.year}</td>
																<td>${book.description}</td>
																<td>${book.translater}</td>
																<ui:abbr title="abbr.plus">
																	<td>
																		<a class="importClass btn btn-primary"><ui:glyph name="plus" /></a>
																	</td>
																</ui:abbr>
															<tr>
														</c:forEach>
														</tbody>
													</table>
												</div>
											</div>
									</div>
									<div class="modal-footer">
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			<c:url var="orderhref" value="/order/newOrder"/>
			<form:form action="${orderhref}" method="POST" id="orderform"
				modelAttribute="order" enctype="application/form-data" >
				<form:input type="hidden" path="id" />
				<table class="table table-condensed none-margin">
					<tr>
						<td width="20%"><ui:label value="order.name" /></td>
						<td width="80%" colspan="3"><ui:input name="name"
							placeholder="order.name.placeholder" cssClass="form-control"
							required="required" value="${order.name}" /></td>
					</tr>
					<tr>
						<td width="20%"><ui:label value="order.date" /></td>
						<td width="70%">
							<ui:date name="date"
								placeholder="order.date.placeholder"
								cssClass="form-control"
								required="required"
								value="${order.date}" />
						</td>
					</tr>
				</table>
				<div class="padding-for-table">
					<table id="orderItem" class="table table-hover">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col">Name</th>
								<th scope="col">Genre</th>
								<th scope="col">Author</th>
								<th scope="col">Publisher</th>
								<th scope="col">Year</th>
								<th scope="col">Description</th>
								<th scope="col">Translator</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody id="items">
						</tbody>
					</table>
				</div>
				<ui:panel-footer>
					<div class="btn-group">
						<ui:buttonSubmit name="save Order" type="primary" path="submit" />
					</div>
				</ui:panel-footer>
			</form:form>
			</ui:panel>
		</div>
	</div>
</body>
</ui:html>
