<%@ page contentType="text/html; charset=UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ui"%>
<ui:html title="Orders">
<ui:navbar active="orders order.orders" />
<ui:panel>
	<ui:panel-header>
		<form class="form-inline">
			<div class="row">
				<div class="form-group col-md-offset-2 col-md-8" align="center">
					<h4>
						<ui:label value="orders" />
					</h4>
				</div>
			</div>
		</form>
	</ui:panel-header>
	<div style= "position: fixed;right: 10px;top: 20%;padding: 10px;">
		<ui:abbr title="abbr.up">
			<ui:button href="javascript:void(0)" onClick="window.scrollTo(0,0)" glyph="eject" />
		</ui:abbr>
	</div>
	<div class="row">
		<div class="form-group col-md-10">
	<c:if test="${not empty orders}">
		<table class="table table-hover">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">order.name</th>
					<th scope="col">order.date</th>
					<th scope="col">action</th>
				</tr>
			</thead>
			<c:forEach var="order" items="${orders}">
				<tr>
					<th scope="row">
						<c:url value="/order/edit/${order.id}" var="href" />
						<a href="${href}">${order.id}</a></th>
					<td>${order.name}</td>
					<td>${order.date}</td>
					<ui:abbr title="abbr.delete">
						<td><c:url value="/order/delete/${order.id}" var="href" /> <a
							href="${href}" class="btn btn-danger" onclick="return confirm('Are you sure?')"><ui:glyph name="trash" /></a>
						</td>
					</ui:abbr>
				<tr>
			</c:forEach>
		</table>
	</c:if>
	</div>
		</div>
	<ui:panel-footer>
	</ui:panel-footer>
</ui:panel>
</ui:html>
