<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ui"%>
<%@ attribute name="name" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="glyph" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="cssClass" required="false" %>
<li class="dropdown ${cssClass}"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
    aria-expanded="false"> 
        <ui:glyph name="${glyph}" />
        <spring:message code="${name}" text="${name}" />
        <span class="caret"></span></a>
    <ul class="dropdown-menu">
        <jsp:doBody />
    </ul>
</li>
