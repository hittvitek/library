<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ attribute name="name" required="true" %>
<c:choose>
    <c:when test="${fn:contains(name, 'fa-')}">
        <span class="fa ${name}" aria-hidden="true" ></span>
    </c:when>
    <c:otherwise>
        <span class="glyphicon glyphicon-${name}" aria-hidden="true" ></span>
    </c:otherwise>
</c:choose>