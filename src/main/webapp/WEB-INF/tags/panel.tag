<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ui"%>
<%@ attribute name="type" %>
<%@ attribute name="title" %>
<div class="panel panel-${empty type ? 'default' : type}" >
    <c:if test="${not empty title}">
        <div class="panel-heading text-center">
            <h4><ui:label value="${title}"/></h4>
        </div>
    </c:if>
    <jsp:doBody/>
</div>
