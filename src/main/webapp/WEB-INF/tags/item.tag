<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ui"%>
<%@ attribute name="href" required="true" type="java.lang.String" %>
<%@ attribute name="name" required="true" type="java.lang.String" %>
<%@ attribute name="glyph" required="false" type="java.lang.String" %>
<%@ attribute name="cssClass" required="false" %>
<%@ attribute name="onclick" required="false" %>

<li class="${cssClass}">
    <c:url value="${href}" var="url" />
    <a href="${url}" onclick="${empty onclick ? '': onclick}" >
        <ui:glyph name="${glyph}" /><spring:message code="${name}" text="${name}" />
    </a>
    <jsp:doBody />
</li>
