<%@ tag %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ attribute name="name" required="true"%>
<%@ attribute name="cssClass" required="false"%>
<%@ attribute name="placeholder" required="false"%>
<%@ attribute name="required" required="false"%>
<%@ attribute name="value" required="false" type="java.util.Date"%>
<%@ attribute name="formatPattern" required="false" %>
<spring:message code="${placeholder}" text="${placeholder}" var="_placeholder"/>
<fmt:formatDate value="${value}" pattern="${empty formatPattern ? 'yyyy-MM-dd' : formatPattern}" var="date"/>
<input type="date" class="${cssClass}" name="${name}" placeholder="${_placeholder}" required="${empty required ? 'false' : required}" value="${date}"/>