<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ attribute name="name" required="true"%>
<%@ attribute name="cssClass" required="false"%>
<%@ attribute name="placeholder" required="false"%>
<%@ attribute name="required" required="false"%>
<%@ attribute name="value" required="false"%>
<%@ attribute name="size" required="false"%>
<%@ attribute name="type" required="false"%>
<spring:message code="${placeholder}" text="${placeholder}" var="_placeholder"/>
<input type="${empty type ? 'text' : type}" class="${cssClass}" name="${name}" placeholder="${_placeholder}" required="${empty required ? 'false' : required}" value="${empty value ? '' : value}" size="${size}" />
