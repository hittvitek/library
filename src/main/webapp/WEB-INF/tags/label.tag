<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ attribute name="value" required="true" %>
<%@ attribute name="cssClass" required="false" %>
<%@ attribute name="_for" required="false"%>
<spring:message code="${value}" text="${value}" var="_value"/>
<label class="${cssClass}" for="${_for}">${_value}</label>
