<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ui"%>
<%@ attribute name="active" %>
<!-- Классы navbar и navbar-default (базовые классы меню) -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Контейнер (определяет ширину Navbar) -->
    <div class="container-fluid">
        <!-- Заголовок -->
        <div class="navbar-header">
            <!-- Кнопка «Гамбургер» отображается только в мобильном виде (предназначена для открытия основного содержимого Navbar) -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- Бренд или название сайта (отображается в левой части меню) -->
            <c:url value="/welcome" var="href" /><a class="navbar-brand" href="${href}"><span><c:url value="/img/schedule_w.svg" var="src"/><img src="${src}" width="19"></span>Brand</a>
        </div>
        <!-- Основная часть меню (может содержать ссылки, формы и другие элементы) -->
        <div class="collapse navbar-collapse" id="navbar-main">
          <!-- Содержимое основной части -->
              <ul class="nav navbar-nav">
                  <ui:dropdown name="books" glyph="book" cssClass="btn-group inline">
                      <ui:item href="/book/newBook" name="add" glyph="plus" />
                      <ui:item href="/book/books" name="show.all" glyph="eye-open" />
                      <ui:item href="/book/freeBooks" name="show.free" glyph="eye-open" />
                  </ui:dropdown>
                  <ui:dropdown name="orders" glyph="order" cssClass="btn-group inline">
                      <ui:item href="/order/newOrder" name="add" glyph="plus" />
                      <ui:item href="/order/orders" name="show.all" glyph="eye-open" />
                  </ui:dropdown>
                  <c:if test="${not empty owner}">
                      <ui:dropdown name="ownerProducts" glyph="user" cssClass="btn-group inline">
                          <ui:item href="/owner/${owner.id}/newProduct" name="add" glyph="plus" />
                          <ui:item href="/product/products" name="show.all" glyph="eye-open" />
                      </ui:dropdown>
                  </c:if>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                  <ui:dropdown name="nav.language">
                      <ui:item href="?language=en" name="nav.english" />
                      <ui:item href="?language=ru" name="nav.russian" />
                  </ui:dropdown>
              </ul>
        </div>
    </div>
</nav>
