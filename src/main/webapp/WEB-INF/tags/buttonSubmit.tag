<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ attribute name="name" required="false" %>
<%@ attribute name="type" required="false" %>
<%@ attribute name="value" required="false" %>
<%@ attribute name="path" required="false" %>
<spring:message code="${name}" text="${name}" var="_name"/>
<button type="submit" name="${path}" class="btn btn-${empty type ? 'default' : type}" value="${empty value ? '' : value}">${_name}</button>
