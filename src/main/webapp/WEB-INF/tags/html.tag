<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute name="title"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<c:if test="${not empty title}">
		<title>${title}</title>
	</c:if>
	<c:url value="/css/bootstrap.min.css" var="href" />
	<link href="${href}" rel="stylesheet">
	<c:url value="/css/main.css" var="href" />
	<link href="${href}" rel="stylesheet">
	<script src="<c:url value="/js/jquery-1.11.0.min.js" />"></script>
	<script src="<c:url value="/js/bootstrap.min.js" />"></script>
	<script src="<c:url value="/js/order.js" />"></script>
	<script>
		function adjust_body_offset() {
			$('body').css('padding-top', $('.navbar').outerHeight(true) + 'px');
		}
		$(window).resize(adjust_body_offset);
		$(document).ready(adjust_body_offset);
	</script>
</head>
<body>
	<jsp:doBody />
	<nav class="navbar navbar-default navbar-fixed-bottom">
		<div class="container">
			<div class="row pull-right">
				<span class="label label-primary" style="line-height: 44px">©2019</span>
			</div>
		</div>
	</nav>
</body>
</html>
