<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ui"%>
<%@ attribute name="href" required="true"%>
<%@ attribute name="id" required="false"%>
<%@ attribute name="name" required="false"%>
<%@ attribute name="type"%>
<%@ attribute name="glyph" %>
<%@ attribute name="onClick" %>
<spring:message code="${name}" text="${name}" var="_name"/>
<c:url value="${href}" var="_href" />
<a class="btn btn-${empty type ? 'default' : type}" href="${_href}" type="button" onclick="${onClick}">
	<ui:glyph name="${glyph}" /> ${_name}</a>
