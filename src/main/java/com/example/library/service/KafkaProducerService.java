package com.example.library.service;

import com.example.library.po.Book;

public interface KafkaProducerService {

    void sendOrder(Book book);

}
