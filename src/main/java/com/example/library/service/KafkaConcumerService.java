package com.example.library.service;

import org.apache.kafka.clients.consumer.ConsumerRecord;

import com.example.library.po.Book;

public interface KafkaConcumerService {

    void orderListener(ConsumerRecord<Long, Book> record);

    void saveBook(Book book);

}
