package com.example.library.service;

import java.util.List;

import com.example.library.po.Book;

public interface BookService {

    List<Book> getBooks();

    List<Book> getFreeBooks();

}
