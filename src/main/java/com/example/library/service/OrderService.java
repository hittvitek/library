package com.example.library.service;

import java.util.List;

import com.example.library.po.Order;

public interface OrderService {

    List<Order> getOrders();

    void save(Order order);

    void deleteOrderItem(Long orderId, Long bookId);

    void deleteOrder(Long orderId);

    Order getOrderById(Long orderId);

}
