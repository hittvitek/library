package com.example.library.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.library.po.Order;
import com.example.library.repository.dao.OrderDao;
import com.example.library.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderDao orderDao;

    @Override
    public List<Order> getOrders() {
        return orderDao.fillOrders();
    }

    @Override
    public void save(Order order) {
        orderDao.create(order);
    }

    @Override
    public void deleteOrderItem(Long orderId, Long bookId) {
        orderDao.deleteOrderItem(orderId, bookId);
    }

    @Override
    public void deleteOrder(Long orderId) {
        orderDao.deleteOrder(orderId);
    }

    @Override
    public Order getOrderById(Long orderId) {
        return orderDao.read(orderId);
    }

}
