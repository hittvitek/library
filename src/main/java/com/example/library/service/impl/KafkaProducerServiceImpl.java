package com.example.library.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

import com.example.library.po.Book;
import com.example.library.service.KafkaProducerService;

@Service
public class KafkaProducerServiceImpl implements KafkaProducerService {

    @Autowired
    private KafkaTemplate<Long, Book> kafkaTemplate;

    @Override
    public void sendOrder(Book book) {
        ListenableFuture<SendResult<Long, Book>> future = kafkaTemplate.send("book", book);
        future.addCallback(System.out::println, System.err::println);
        kafkaTemplate.flush();
    }

}
