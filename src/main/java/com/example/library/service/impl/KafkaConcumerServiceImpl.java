package com.example.library.service.impl;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.library.po.Book;
import com.example.library.repository.dao.BookDao;
import com.example.library.service.KafkaConcumerService;

@Service
public class KafkaConcumerServiceImpl implements KafkaConcumerService {

    @Autowired
    BookDao bookDao;

    @Override
    public void saveBook(final Book book) {
        bookDao.create(book);
    }

    @Override
    public void orderListener(ConsumerRecord<Long, Book> record) {
        System.out.println(record.partition());
        System.out.println(record.key());
        System.out.println(record.value());
    }

}
