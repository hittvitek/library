package com.example.library.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.library.po.Book;
import com.example.library.repository.dao.BookDao;
import com.example.library.service.BookService;

@Service
public class BookServiceImpl implements BookService{

    @Autowired
    BookDao bookDao;

    @Override
    public List<Book> getBooks() {
        return bookDao.fillBooks();
    }

    @Override
    public List<Book> getFreeBooks() {
        return bookDao.fillFreeBooks();
    }

}
