package com.example.library.repository.dao.impl;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.example.library.po.Book;
import com.example.library.po.Order;
import com.example.library.repository.dao.OrderDao;

@Repository
public class OrderDaoImpl extends JdbcDaoSupport implements OrderDao {

    @Autowired
    DataSource dataSource;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public void create(Order model) {
        Long order_id = null;
        String sql = "INSERT INTO ORDERS (name, date) VALUES (?, ?)";
        getJdbcTemplate().update(sql, new Object[] { model.getName(), model.getDate() });

        sql = "SELECT currval('public.orders_id_seq'::regclass) AS order_id";
        order_id = getJdbcTemplate().queryForObject(sql, new RowMapper<Long>() {
            @Override
            public Long mapRow(ResultSet rs, int rwNumber) throws SQLException {
                return rs.getLong("order_id");
            }
        });

        sql = "INSERT INTO orderitems (order_id, book_id) VALUES (?, ?)";
        for (Book item : model.getBooks()) {
            if (item.getId() != null) {
                getJdbcTemplate().update(sql, new Object[] { order_id, item.getId() });
            }
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public Order read(Long id) {
        String sql = "SELECT ORD.ID, ORD.DATE, ORD.NAME, BK.ID, BK.NAME, BK.DESCRIPTION, BK.AUTHOR, BK.GENRE, BK.PUBLISHER, BK.TRANSLATER, BK.YEAR FROM ORDERS ORD " + 
                       "JOIN ORDERITEMS ORI ON ORD.ID = ORI.ORDER_ID " +
                       "JOIN BOOKS BK ON ORI.BOOK_ID = BK.ID " +
                       "WHERE ORD.ID = ?";
        return (Order) getJdbcTemplate().queryForObject(sql, new Object[] { id}, new RowMapper<Order>() {
            @Override
            public Order mapRow(ResultSet rs, int rwNumber) throws SQLException {
                List<Book> books = new ArrayList<>();
                Book book = null;
                Order order = new Order();
                order.setId((Long)rs.getLong(1));
                order.setDate((Date)rs.getDate(2));
                order.setName((String)rs.getString(3));
                book = new Book();
                book.setId((Long)rs.getLong(4));
                book.setName((String)rs.getString(5));
                book.setDescription((String)rs.getString(6));
                book.setAuthor((String)rs.getString(7));
                book.setGenre((String)rs.getString(8));
                book.setPublisher((String)rs.getString(9));
                book.setTranslater((String)rs.getString(10));
                book.setYear((Date)rs.getDate(11));
                books.add(book);
                while (rs.next()) {
                    book = new Book();
                    book.setId((Long)rs.getLong(4));
                    book.setName((String)rs.getString(5));
                    book.setDescription((String)rs.getString(6));
                    book.setAuthor((String)rs.getString(7));
                    book.setGenre((String)rs.getString(8));
                    book.setPublisher((String)rs.getString(9));
                    book.setTranslater((String)rs.getString(10));
                    book.setYear((Date)rs.getDate(11));
                    books.add(book);
                }
                order.setBooks(books);
                return order;
            }
        });
    }

    @Override
    public void update(Order model) {
        // TODO Auto-generated method stub

    }

    @Override
    public void delete(Long id) {
        // TODO Auto-generated method stub

    }

    @Override
    public List<Order> fillOrders() {
        String sql = "SELECT * FROM ORDERS";
        List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

        List<Order> result = new ArrayList<Order>();
        Order order = null;
        for (Map<String, Object> row : rows) {
            order = new Order();
            order.setId((Long) row.get("id"));
            order.setName((String) row.get("name"));
            order.setDate((Date) row.get("date"));
            result.add(order);
            order = null;
        }

        return result;
    }

    @Override
    public void deleteOrderItem(Long orderId, Long bookId) {
        String sql = "DELETE FROM orderitems WHERE order_id = ? AND book_id = ?";
        getJdbcTemplate().update(sql, new Object[] {orderId, bookId});
    }

    @Override
    public void deleteOrder(Long orderId) {
        String sql = "DELETE FROM orderitems WHERE order_id = ?";
        getJdbcTemplate().update(sql, new Object[] {orderId});

        sql = "DELETE FROM orders WHERE id = ?";
        getJdbcTemplate().update(sql, new Object[] {orderId});
    }

}
