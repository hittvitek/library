package com.example.library.repository.dao.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.example.library.po.Book;
import com.example.library.repository.dao.BookDao;

@Repository
public class BookDaoImpl extends JdbcDaoSupport implements BookDao {

    @Autowired
    DataSource dataSource;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public void create(Book model) {
        String sql = "INSERT INTO BOOKS (name, description, author, genre, publisher, translater, year) VALUES (?, ?, ?, ?, ?, ?, ?)";
        getJdbcTemplate().update(sql,
            new Object[] { model.getName(),
            model.getDescription(),
            model.getAuthor(),
            model.getGenre(),
            model.getPublisher(),
            model.getTranslater(),
            model.getYear()}
        );

    }

    @Override
    public Book read(Long id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void update(Book model) {
        // TODO Auto-generated method stub

    }

    @Override
    public void delete(Long id) {
        // TODO Auto-generated method stub

    }

    @Override
    public List<Book> fillBooks() {
        String sql = "SELECT * FROM BOOKS";
        List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

        List<Book> result = new ArrayList<Book>();
        Book book = null;
        for (Map<String, Object> row : rows) {
            book = new Book();
            book.setId((Long) row.get("id"));
            book.setName((String) row.get("name"));
            book.setDescription((String) row.get("description"));
            book.setAuthor((String) row.get("author"));
            book.setGenre((String) row.get("genre"));
            book.setPublisher((String) row.get("publisher"));
            book.setTranslater((String) row.get("translater"));
            book.setYear((Date) row.get("year"));
            result.add(book);
            book = null;
        }

        return result;
    }

    @Override
    public List<Book> fillFreeBooks() {
        String sql = "SELECT BK.ID AS ID, BK.NAME AS NAME, BK.DESCRIPTION AS DESCRIPTION, BK.AUTHOR AS AUTHOR, BK.GENRE AS GENRE, BK.PUBLISHER AS PUBLISHER, BK.TRANSLATER AS TRANSLATER, BK.YEAR AS YEAR FROM BOOKS BK " +
            " LEFT JOIN ORDERITEMS ORI ON BK.ID = ORI.BOOK_ID " +
            " LEFT JOIN ORDERS ORD ON ORD.ID = ORI.ORDER_ID " +
            " WHERE ORD.DATE IS NULL OR ORD.DATE <= CURRENT_DATE";
        List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

        List<Book> result = new ArrayList<Book>();
        Book book = null;
        for (Map<String, Object> row : rows) {
            book = new Book();
            book.setId((Long) row.get("ID"));
            book.setName((String) row.get("name"));
            book.setDescription((String) row.get("description"));
            book.setAuthor((String) row.get("author"));
            book.setGenre((String) row.get("genre"));
            book.setPublisher((String) row.get("publisher"));
            book.setTranslater((String) row.get("translater"));
            book.setYear((Date) row.get("year"));
            result.add(book);
            book = null;
        }
        return result;
    }

}
