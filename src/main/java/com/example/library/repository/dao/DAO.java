package com.example.library.repository.dao;

public interface DAO<T, O> {

    void create(O model);

    O read(T id);

    void update(O model);

    void delete(T id);

}
