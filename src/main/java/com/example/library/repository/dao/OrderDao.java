package com.example.library.repository.dao;

import java.util.List;

import com.example.library.po.Order;

public interface OrderDao extends DAO<Long, Order> {

    List<Order> fillOrders();

    void deleteOrderItem(Long orderId, Long bookId);

    void deleteOrder(Long orderId);

}
