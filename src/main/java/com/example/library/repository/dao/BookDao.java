package com.example.library.repository.dao;

import java.util.List;

import com.example.library.po.Book;

public interface BookDao extends DAO<Long, Book>{

    List<Book> fillBooks();

    List<Book> fillFreeBooks();

}
