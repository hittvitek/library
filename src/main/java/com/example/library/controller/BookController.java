package com.example.library.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.library.po.Book;
import com.example.library.service.BookService;
import com.example.library.service.KafkaProducerService;

@RestController
@RequestMapping(value="/book")
public class BookController {

    @Autowired
    private KafkaProducerService kafkaProducerService;
    
    @Autowired
    private BookService bookService;

    @GetMapping(value = "/newBook")
    public ModelAndView show(final Model model) {
        Book book = new Book();
        return new ModelAndView("book/add", "book", book);
    }

    @PostMapping(value = "/add")
    public ModelAndView sendOrder(@ModelAttribute("book") final Book book, final Model model) {
        kafkaProducerService.sendOrder(book);
        return new ModelAndView("/welcome");
    }

    @RequestMapping("/books")
    public ModelAndView list(final Model model) {
        model.addAttribute("books", bookService.getBooks());
        return new ModelAndView("book/books");
    }

    @RequestMapping("/freeBooks")
    public ModelAndView listFree(final Model model) {
        model.addAttribute("books", bookService.getFreeBooks());
        return new ModelAndView("book/freeBooks");
    }
    
}
