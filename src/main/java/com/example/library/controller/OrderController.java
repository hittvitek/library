package com.example.library.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.library.po.Book;
import com.example.library.po.Order;
import com.example.library.service.BookService;
import com.example.library.service.OrderService;

@RestController
public class OrderController {

    @Autowired
    OrderService orderService;

    @Autowired
    BookService bookService;

    @GetMapping(value = "/order/newOrder")
    public ModelAndView show(final Model model) {
        List<Book> books = bookService.getFreeBooks();
        model.addAttribute("books", books);
        return new ModelAndView("order/add", "order", new Order());
    }
   
    @PostMapping(value = "/order/newOrder")
    public ModelAndView saveOrder(@ModelAttribute("order") final Order order, final Model model) {
        orderService.save(order);
        return new ModelAndView("order/orders");
    }

    @RequestMapping("/order/orders")
    public ModelAndView list(final Model model) {
        model.addAttribute("orders", orderService.getOrders());
        return new ModelAndView("order/orders");
    }

    @GetMapping(value = "/order/{orderId}/delete/orderItem/{bookId}")
    public void orderDeleteItem(@PathVariable final Long orderId, @PathVariable("bookId") final Long bookId) {
        orderService.deleteOrderItem(orderId, bookId);
    }

    @GetMapping(value = "/order/delete/{orderId}")
    public ModelAndView orderDelete(@PathVariable final Long orderId) {
        orderService.deleteOrder(orderId);
        return new ModelAndView("order/orders");
    }

    @GetMapping(value = "/order/edit/{orderId}")
    public ModelAndView ownerEdit(final Model model, @PathVariable final Long orderId) {
        Order order = orderService.getOrderById(orderId);
        List<Book> freebooks = bookService.getFreeBooks();
        model.addAttribute("freebooks", freebooks);
        model.addAttribute(order);
        return new ModelAndView("order/edit");
    }
    
}
