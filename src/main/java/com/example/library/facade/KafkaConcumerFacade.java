package com.example.library.facade;

import org.apache.kafka.clients.consumer.ConsumerRecord;

import com.example.library.po.Book;

public interface KafkaConcumerFacade {

    void orderListenerAndSaveTopic(ConsumerRecord<Long, Book> record);

}
