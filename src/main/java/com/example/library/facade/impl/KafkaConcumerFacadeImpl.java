package com.example.library.facade.impl;

import java.text.SimpleDateFormat;
import java.sql.Date;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.example.library.facade.KafkaConcumerFacade;
import com.example.library.po.Book;
import com.example.library.service.KafkaConcumerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import kafka.utils.Json;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;

@Component
public class KafkaConcumerFacadeImpl implements KafkaConcumerFacade {

    @Autowired
    KafkaConcumerService kafkaConcumerService;

    @Override
    @KafkaListener(topics = "book")
    public void orderListenerAndSaveTopic(ConsumerRecord<Long, Book> record) {
        kafkaConcumerService.orderListener(record);
        ObjectMapper objectMapper = new ObjectMapper();
        String book = new String();
        try {
            book = objectMapper.writeValueAsString(record.value());
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Book bookObj = null;
        try {
            bookObj = objectMapper.readValue(book, Book.class);
        } catch (JsonMappingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        kafkaConcumerService.saveBook(bookObj);
    }

}
