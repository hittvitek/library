-- Table: public.books

-- DROP TABLE public.books;

CREATE TABLE public.books
(
  id bigint NOT NULL DEFAULT nextval('books_id_seq'::regclass),
  name character varying(126),
  description character varying(1024),
  author character varying(256),
  genre character varying(126),
  publisher character varying(256),
  translater character varying(256),
  year date,
  CONSTRAINT pk_book PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.books
  OWNER TO postgres;

-- Table: public.orders

-- DROP TABLE public.orders;

CREATE TABLE public.orders
(
  id bigint NOT NULL DEFAULT nextval('orders_id_seq'::regclass),
  date date,
  name character varying(126),
  CONSTRAINT order_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.orders
  OWNER TO postgres;

-- Table: public.orderitems

-- DROP TABLE public.orderitems;

CREATE TABLE public.orderitems
(
  order_id bigint NOT NULL,
  book_id bigint NOT NULL,
  CONSTRAINT "orderItems_pk" PRIMARY KEY (order_id, book_id),
  CONSTRAINT fk_order_book FOREIGN KEY (book_id)
      REFERENCES public.books (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "fk_order_orderItem" FOREIGN KEY (order_id)
      REFERENCES public.orders (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.orderitems
  OWNER TO postgres;
